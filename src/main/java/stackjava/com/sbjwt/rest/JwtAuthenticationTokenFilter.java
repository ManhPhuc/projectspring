
package stackjava.com.sbjwt.rest;

import java.io.Console;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import stackjava.com.sbjwt.service.JwtService;
import stackjava.com.sbjwt.service.UserService;

public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

	private final static String AUTH_HEADER = "Authorization";

	@Autowired
	private JwtService jwtService;

	@Autowired
	private UserService userService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String authToken = null;
		String urlLoging = "/api/login";
		String authHeader = httpRequest.getHeader(AUTH_HEADER);
		String urlString = httpRequest.getServletPath();
		if (urlLoging.equals(urlString)) {
			chain.doFilter(request, response);
		} else {
			System.out.println("token : " + authHeader + " | url: " + httpRequest.getRequestURL());
			if (authHeader != null && authHeader.startsWith("Bearer ")) {
				authToken = authHeader.substring(7);
			}

			if (jwtService.validateTokenLogin(authToken)) {
				String username = jwtService.getUsernameFromToken(authToken);

				stackjava.com.sbjwt.entities.SsUserEntity user = userService.loadUserByUsername(username);
				if (user != null) {
					boolean enabled = true;
					boolean accountNonExpired = true;
					boolean credentialsNonExpired = true;
					boolean accountNonLocked = true;
					UserDetails userDetail = new User(username, user.getPassword(), enabled, accountNonExpired,
							credentialsNonExpired, accountNonLocked, user.getAuthorities());

					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetail, null, userDetail.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}

			chain.doFilter(request, response);
		}
	}
}
