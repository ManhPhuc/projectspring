package stackjava.com.sbjwt.entities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "ss_user", schema = "kis_style_seat2")
//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value = { "authorities", "encPassword" })
public class SsUserEntity{
    private int uid;
    private String email;
    private String password;
    private UserStatus status;
    private String phone;
    private byte isDisable;
    private UserType type;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    public SsUserEntity() {
    }

    public SsUserEntity(int uid, String email, String password) {
        this.uid = uid;
        this.email = email;
        this.password = password;
    }

    @Transient
    public List<GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(type.toString()));
        return authorities;
    }

    @Id
    @Column(name = "uid", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "email", nullable = false, unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "enc_password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "is_disable")
    public byte getIsDisable() {
        return isDisable;
    }

    public void setIsDisable(byte isDisable) {
        this.isDisable = isDisable;
    }

    @Basic
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    @Basic
    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

	@Override
	public String toString() {
		return "SsUserEntity [uid=" + uid + ", email=" + email + ", password=" + password + ", status=" + status
				+ ", phone=" + phone + ", isDisable=" + isDisable + ", type=" + type + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + "]";
	} 
}
