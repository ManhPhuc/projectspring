package stackjava.com.sbjwt.entities;

public enum  UserType {
	ROLE_GENERALMANAGER, ROLE_MANAGER, 	
	ROLE_WORKER, ROLE_CLIENT
}
