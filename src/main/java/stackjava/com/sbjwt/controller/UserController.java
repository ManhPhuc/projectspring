package stackjava.com.sbjwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stackjava.com.sbjwt.entities.SsUserEntity;
import stackjava.com.sbjwt.service.JwtService;
import stackjava.com.sbjwt.service.ServiceResult;
import stackjava.com.sbjwt.service.UserService;

	
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api")
public class UserController {

    @Autowired
    private JwtService jwtService;

    @Autowired
    UserService userService;		

    @GetMapping(path = "/users")
    public ResponseEntity<List<SsUserEntity>> findAllProfile() {
        return  new ResponseEntity<List<SsUserEntity>>(userService.allUser(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<ServiceResult> findByid(@PathVariable int id){
        return  new ResponseEntity<ServiceResult>(userService.findByid(id), HttpStatus.OK);
    }

    @PostMapping(path ="/users" , consumes = {MediaType.APPLICATION_JSON_VALUE},
    		produces = { MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ServiceResult> create(@ModelAttribute("ssUserEntityFormData") SsUserEntity ssUserEntity, ModelMap model){
    	System.out.println("Object user create: " + ssUserEntity.toString());
        return  new ResponseEntity<ServiceResult>(userService.create(ssUserEntity), HttpStatus.OK);
    }

    @PutMapping("/users")
    public ResponseEntity<ServiceResult> update(@RequestBody SsUserEntity ssUserEntity){
        return  new ResponseEntity<ServiceResult>(userService.update(ssUserEntity), HttpStatus.OK);
    }

    @DeleteMapping("/users")
    public ResponseEntity<ServiceResult> delete(@PathVariable int id) {	
        return new ResponseEntity<ServiceResult>(userService.delete(id), HttpStatus.OK); 
    }

    @PostMapping(value = "/login" , consumes = {MediaType.APPLICATION_JSON_VALUE},
    		produces = { MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ServiceResult> login(@RequestBody SsUserEntity user) {  
    	System.out.println("user" + user.toString());
        ServiceResult result = userService.checkLogin(user);
        try {
            if (result.getData() != null) {
                result.setMessage(jwtService.generateTokenLogin(user.getEmail()));
                result.setStatus(ServiceResult.Status.SUCCESS);
            } else {
                result.setMessage("Wrong userId and password");
                result.setStatus(ServiceResult.Status.FALLED);
            }
        } catch (Exception ex) {
            result.setMessage("Server Error");
            result.setStatus(ServiceResult.Status.FALLED);
        }
        return new ResponseEntity<ServiceResult>(result, HttpStatus.OK);
    }
}