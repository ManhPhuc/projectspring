package stackjava.com.sbjwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import stackjava.com.sbjwt.entities.SsProfileEntity;
import stackjava.com.sbjwt.entities.SsUserEntity;
import stackjava.com.sbjwt.service.ProfileService;
import stackjava.com.sbjwt.service.ServiceResult;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api")	
public class ProfileController {
    @Autowired
    ProfileService profileService;
    
    @GetMapping("/profiles")
    public ResponseEntity<List<SsProfileEntity>> findAllProfile() {
        return  new ResponseEntity<List<SsProfileEntity>>(profileService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/profiles/{id}")
    public ResponseEntity<ServiceResult> findByid(@PathVariable int id){
        return  new ResponseEntity<ServiceResult>(profileService.findByid(id), HttpStatus.OK);
    }

    @PostMapping("/profiles")
    public ResponseEntity<ServiceResult> create(@RequestBody SsProfileEntity ssProfileEntity){
        return  new ResponseEntity<ServiceResult>(profileService.create(ssProfileEntity), HttpStatus.OK);
    }

    @PutMapping("/profiles/{id}")
    public ResponseEntity<ServiceResult> update( @PathVariable int id , @ModelAttribute SsProfileEntity ssProfileEntity){
        return  new ResponseEntity<ServiceResult>(profileService.update(ssProfileEntity), HttpStatus.OK);
    }

    @DeleteMapping("/profiles")
    public ResponseEntity<ServiceResult> delete(@PathVariable int id) {
        return new ResponseEntity<ServiceResult>(profileService.delete(id), HttpStatus.OK);
    }
}
