package stackjava.com.sbjwt.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stackjava.com.sbjwt.entities.SsUserEntity;
import stackjava.com.sbjwt.repositories.UserRepository;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public String encode(String input) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {

        }
        return sha1;
    }

    public ServiceResult findAll() {
        ServiceResult result = new ServiceResult();
        result.setData(userRepository.findAll());
        return result;
    }

    public List<SsUserEntity> allUser() {
        List<SsUserEntity> listuser = new ArrayList<>();
        ServiceResult result = new ServiceResult(); 
        result.setData(userRepository.findAll());
        listuser = (List<SsUserEntity>) result.getData();
        return listuser;
    }

    public ServiceResult findByid(int id) {
        ServiceResult result = new ServiceResult();
        SsUserEntity ssUserEntity = userRepository.findById(id).orElse(null);
        result.setData(ssUserEntity);	

        return result;
    }

    public ServiceResult create(SsUserEntity ssUserEntity) {
        ServiceResult result = new ServiceResult();
        SsUserEntity user = loadUserByUsername(ssUserEntity.getEmail());
        if (user == null){
            String encpass =  this.encode(ssUserEntity.getPassword());
            ssUserEntity.setPassword(encpass);
            result.setData(userRepository.save(ssUserEntity));
        }else{
            result.setStatus(ServiceResult.Status.FALLED);
            result.setMessage("Email incorrect!");
        }
        return result;
    }

    public ServiceResult update(SsUserEntity ssUserEntity) {
        ServiceResult result = new ServiceResult();
        if (!userRepository.findById(ssUserEntity.getUid()).isPresent()) {
            result.setStatus(ServiceResult.Status.FALLED);
            result.setMessage("User Not Found");
        } else {
            result.setData(userRepository.save(ssUserEntity));
            result.setMessage("Update User Success");
        }
        return result;
    }

    public ServiceResult delete(int id) {
        ServiceResult result = new ServiceResult();

        SsUserEntity ssUserEntity = userRepository.findById(id).orElse(null);
        if (ssUserEntity == null) {
            result.setStatus(ServiceResult.Status.FALLED);
            result.setMessage("User Not Found");
        } else {
            userRepository.delete(ssUserEntity);
            result.setMessage("Delete User Success");
        }
        return result;
    }

    public SsUserEntity loadUserByUsername(String email) {
        for (SsUserEntity user : this.allUser()) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        return null;
    }

    public ServiceResult checkLogin(SsUserEntity user) {
    	ServiceResult result = new ServiceResult();
        String encpass =  this.encode(user.getPassword());
        for (SsUserEntity userExist : this.allUser()) {
            if (StringUtils.equals(user.getEmail(), userExist.getEmail())
                    && StringUtils.equals( encpass, userExist.getPassword())) {
                result.setData(userExist);
            }
        }
        return result;
    }
}