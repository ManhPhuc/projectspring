package stackjava.com.sbjwt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import stackjava.com.sbjwt.entities.SsProfileEntity;
import stackjava.com.sbjwt.entities.SsUserEntity;
import stackjava.com.sbjwt.repositories.ProfileRepository;



@Service
public class ProfileService {

    @Autowired
    ProfileRepository profileRepository;

    public List<SsProfileEntity> findAll() {
        List<SsProfileEntity> listuser = new ArrayList<>();
        ServiceResult result = new ServiceResult(); 
        result.setData(profileRepository.findAll());
        listuser = (List<SsProfileEntity>) result.getData();
        return listuser;
    }

    public ServiceResult findByid(int id) {
        ServiceResult result = new ServiceResult();
        SsProfileEntity ssProfileEntity = profileRepository.findById(id).orElse(null);
        result.setData(ssProfileEntity);
        return result;
    }

    public ServiceResult create(SsProfileEntity ssProfileEntity){
        ServiceResult result = new ServiceResult();
        result.setData(profileRepository.save(ssProfileEntity));
        return result;
    }

    public ServiceResult update(SsProfileEntity ssProfileEntity){
        ServiceResult result = new ServiceResult();
        
//        if(!profileRepository.findById(ssProfileEntity.getUid()).isPresent()){
//            result.setStatus(ServiceResult.Status.FALLED);
//            result.setMessage("Profile Not Found");
//        }else {
//            result.setData(profileRepository.save(ssProfileEntity));
//            result.setMessage("Update Profile Success");
//        }
        result.setData(profileRepository.save(ssProfileEntity));
        result.setMessage("Update Profile Success");
        return  result;
    }

    public ServiceResult delete(int id) {
        ServiceResult result = new ServiceResult();

        SsProfileEntity ssProfileEntity = profileRepository.findById(id).orElse(null);
        if (ssProfileEntity == null) {
            result.setStatus(ServiceResult.Status.FALLED);
            result.setMessage("Profile Not Found");
        } else {
            profileRepository.delete(ssProfileEntity);
            result.setMessage("delete profile success");
        }
        return result;
    }
}

