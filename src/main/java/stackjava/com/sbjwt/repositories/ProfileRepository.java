package stackjava.com.sbjwt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProfileRepository extends JpaRepository<stackjava.com.sbjwt.entities.SsProfileEntity, Integer> {
}
